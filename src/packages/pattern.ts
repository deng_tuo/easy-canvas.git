import type { CanvasRenderingContext2DPlus, Options, Point, Rectangle } from '../types'
import { calculatePoint, connect, connectPoints, mergeOptions } from '../share'

function drawLine(this: CanvasRenderingContext2DPlus, p1: Point, next1: number, next2: number, options?: Options): Point
function drawLine(this: CanvasRenderingContext2DPlus, p1: Point, next1: Point, next2?: Options): Point
function drawLine(this: CanvasRenderingContext2DPlus, p1: any, next1: any, next2?: any, options?: any): Point {
  let p2 = next1

  if (typeof next1 !== 'number') {
    // 传入的参数是两个点
    options = next2
  }
  else {
    // 传入的参数是一个点，长度和角度
    const width = next1
    const angle = next2
    p2 = calculatePoint(p1, width, angle)
  }

  this.save()
  options && mergeOptions(this, options)

  connect(this, p1, p2)
  this.restore()

  return p2
}

function drawLines(this: CanvasRenderingContext2DPlus, points: Point[], options?: Options) {
  this.save()
  options && mergeOptions(this, options)

  connectPoints(this, points)
  this.restore()
}

function drawFillRect(this: CanvasRenderingContext2DPlus, rect: Rectangle, options?: Options) {
  const { x, y, width, height } = rect
  this.save()
  options && mergeOptions(this, options)
  this.fillRect(x, y, width, height)
  this.restore()
}

function drawStrokeRect(this: CanvasRenderingContext2DPlus, rect: Rectangle, options?: Options) {
  const { x, y, width, height } = rect
  this.save()
  options && mergeOptions(this, options)
  this.strokeRect(x, y, width, height)
  this.restore()
}

const patternOps = {
  drawLine,
  drawLines,
  drawFillRect,
  drawStrokeRect,
}

export default patternOps
